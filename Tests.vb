﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Collections
Imports System.Text.RegularExpressions

<TestClass()>
Public Class Tests

    <TestMethod()>
    Public Sub TestMain_Null()

    End Sub

    '<TestMethod()>
    'Public Sub TestStringToArrayList_Null()
    '    Dim inputString As String = ""
    '    Dim expectedResult As New ArrayList

    '    Dim actualResult As ArrayList = StringToArrayList(inputString)

    '    CollectionAssert.AreEqual(expectedResult, actualResult)
    'End Sub

    '<TestMethod()>
    'Public Sub TestStringToArrayList()
    '    Dim inputString As String = "(1+2*2,5)/(3-4)+10,5"
    '    Dim expectedResult As New ArrayList
    '    expectedResult.Add("(")
    '    expectedResult.Add("1")
    '    expectedResult.Add("+")
    '    expectedResult.Add("2")
    '    expectedResult.Add("*")
    '    expectedResult.Add("2,5")
    '    expectedResult.Add(")")
    '    expectedResult.Add("/")
    '    expectedResult.Add("(")
    '    expectedResult.Add("3")
    '    expectedResult.Add("-")
    '    expectedResult.Add("4")
    '    expectedResult.Add(")")
    '    expectedResult.Add("+")
    '    expectedResult.Add("10,5")

    '    Dim actualResult As ArrayList = StringToArrayList(inputString)

    '    CollectionAssert.AreEqual(expectedResult, actualResult, expectedResult.Count & "-" & actualResult.Count)
    'End Sub

    <TestMethod()>
    Public Sub TestStringToMatches()
        Dim inputString As String = "(1+2*2,5)/(3-4)+105"
        Dim actualResult As MatchCollection = StringToMatches(inputString)

        Assert.AreEqual(15, actualResult.Count)
        Assert.AreEqual("(", actualResult(0).ToString)
        Assert.AreEqual("1", actualResult(1).ToString)
        Assert.AreEqual("+", actualResult(2).ToString)
        Assert.AreEqual("2", actualResult(3).ToString)
        Assert.AreEqual("*", actualResult(4).ToString)
        Assert.AreEqual("2,5", actualResult(5).ToString)
        Assert.AreEqual(")", actualResult(6).ToString)
        Assert.AreEqual("/", actualResult(7).ToString)
        Assert.AreEqual("(", actualResult(8).ToString)
        Assert.AreEqual("3", actualResult(9).ToString)
        Assert.AreEqual("-", actualResult(10).ToString)
        Assert.AreEqual("4", actualResult(11).ToString)
        Assert.AreEqual(")", actualResult(12).ToString)
        Assert.AreEqual("+", actualResult(13).ToString)
        Assert.AreEqual("105", actualResult(14).ToString)

    End Sub

    <TestMethod()>
    Public Sub TestGetOperatorPrioritet_Subtraction()
        Dim Symbol As String = "-"
        Dim expectedResult As Integer = 1
        Dim actualResult As Integer = GetOperatorPrioritet(Symbol)

        Assert.AreEqual(expectedResult, actualResult)
    End Sub

    <TestMethod()>
    Public Sub TestGetOperatorPrioritet_Division()
        Dim Symbol As String = "/"
        Dim expectedResult As Integer = 2
        Dim actualResult As Integer = GetOperatorPrioritet(Symbol)

        Assert.AreEqual(expectedResult, actualResult)
    End Sub

    <TestMethod()>
    Public Sub TestGetOperatorPrioritet_NegativeOperation()
        Dim Symbol As String = "E"
        Try
            GetOperatorPrioritet(Symbol)
        Catch ex As ArgumentException
            Return
        Catch
            Assert.Fail("Ошибка исключения для недопустимго символа операции")
            Return
        End Try

        Assert.Fail("Нет исключения на неправильный ввод")
    End Sub

    '<TestMethod()>
    'Public Sub TestBackPolskRec() '(8+2*5)/(1+3*2,5-4)
    '    Dim inputArrayList As New ArrayList
    '    inputArrayList.Add("(")
    '    inputArrayList.Add("8")
    '    inputArrayList.Add("+")
    '    inputArrayList.Add("2")
    '    inputArrayList.Add("*")
    '    inputArrayList.Add("5")
    '    inputArrayList.Add(")")
    '    inputArrayList.Add("/")
    '    inputArrayList.Add("(")
    '    inputArrayList.Add("1")
    '    inputArrayList.Add("+")
    '    inputArrayList.Add("3")
    '    inputArrayList.Add("*")
    '    inputArrayList.Add("2,5")
    '    inputArrayList.Add("-")
    '    inputArrayList.Add("4")
    '    inputArrayList.Add(")")
    '    Dim actualResult As ArrayList = BackPolskRec(inputArrayList)

    '    Dim expectedResult As New ArrayList
    '    expectedResult.Add("8")
    '    expectedResult.Add("2")
    '    expectedResult.Add("5")
    '    expectedResult.Add("*")
    '    expectedResult.Add("+")
    '    expectedResult.Add("1")
    '    expectedResult.Add("3")
    '    expectedResult.Add("2,5")
    '    expectedResult.Add("*")
    '    expectedResult.Add("+")
    '    expectedResult.Add("4")
    '    expectedResult.Add("-")
    '    expectedResult.Add("/")
    '    CollectionAssert.AreEqual(expectedResult, actualResult)
    'End Sub

    '<TestMethod()>
    'Public Sub TestBackPolskRec_Wrong() '(8-(2+2)(
    '    Dim inputArrayList As New ArrayList
    '    inputArrayList.Add("(")
    '    inputArrayList.Add("8")
    '    inputArrayList.Add("-")
    '    inputArrayList.Add("(")
    '    inputArrayList.Add("2")
    '    inputArrayList.Add("+")
    '    inputArrayList.Add("2")
    '    inputArrayList.Add(")")
    '    inputArrayList.Add("(")

    '    Try
    '        BackPolskRec(inputArrayList)
    '    Catch ex As ArgumentException
    '        Return
    '    Catch
    '        Assert.Fail("Ошибка исключения для недопустимого выражения")
    '    End Try

    '    Assert.Fail("Нет исключения на неправильные исходные данные")
    'End Sub

    '<TestMethod()>
    'Public Sub TestBackPolskRec_Wrong2() '(8-)2+2))
    '    Dim inputArrayList As New ArrayList
    '    inputArrayList.Add("(")
    '    inputArrayList.Add("8")
    '    inputArrayList.Add("-")
    '    inputArrayList.Add(")")
    '    inputArrayList.Add("2")
    '    inputArrayList.Add("+")
    '    inputArrayList.Add("2")
    '    inputArrayList.Add(")")
    '    inputArrayList.Add(")")

    '    Try
    '        BackPolskRec(inputArrayList)
    '    Catch ex As ArgumentException
    '        Return
    '    Catch
    '        Assert.Fail("Ошибка исключения для недопустимого выражения")
    '    End Try

    '    Assert.Fail("Нет исключения на неправильные исходные данные")
    'End Sub

    <TestMethod()>
    Public Sub TestBackPolskRec() '(8+2*5)/(1+3*2,5-4)
        Dim InputString As String = "(8+2*5)/(1+3*2,5-4)"
        Dim newReg As Regex = New Regex(pattern)
        Dim inputMatchCollection As MatchCollection = newReg.Matches(InputString)
        Dim actualResult As ArrayList = BackPolskRec(inputMatchCollection)

        Dim expectedResult As New ArrayList
        expectedResult.Add("8")
        expectedResult.Add("2")
        expectedResult.Add("5")
        expectedResult.Add("*")
        expectedResult.Add("+")
        expectedResult.Add("1")
        expectedResult.Add("3")
        expectedResult.Add("2,5")
        expectedResult.Add("*")
        expectedResult.Add("+")
        expectedResult.Add("4")
        expectedResult.Add("-")
        expectedResult.Add("/")
        CollectionAssert.AreEqual(expectedResult, actualResult)
    End Sub

    <TestMethod()>
    Public Sub TestBackPolskRec_Wrong() '(8-(2+2)(
        Dim InputString As String = "(8-(2+2)("
        Dim newReg As Regex = New Regex(pattern)
        Dim inputMatchCollection As MatchCollection = newReg.Matches(InputString)

        Try
            BackPolskRec(inputMatchCollection)
        Catch ex As ArgumentException
            Return
        Catch
            Assert.Fail("Ошибка исключения для недопустимого выражения")
        End Try

        Assert.Fail("Нет исключения на неправильные исходные данные")
    End Sub

    <TestMethod()>
    Public Sub TestBackPolskRec_Wrong2() '(8-)2+2))
        Dim InputString As String = "(8-)2+2))"
        Dim newReg As Regex = New Regex(pattern)
        Dim inputMatchCollection As MatchCollection = newReg.Matches(InputString)

        Try
            BackPolskRec(inputMatchCollection)
        Catch ex As ArgumentException
            Return
        Catch
            Assert.Fail("Ошибка исключения для недопустимого выражения")
        End Try

        Assert.Fail("Нет исключения на неправильные исходные данные")
    End Sub

    <TestMethod()>
    Public Sub TestFunctionCalcInClassSymbol()
        Dim SymbolAddition As New Symbol("+", 1, New OperationAddition)
        Dim expectedResult As Single = 3
        Dim actualResult As Single = SymbolAddition.Calc(1, 2)

        Assert.AreEqual(expectedResult, actualResult)
    End Sub

    <TestMethod()>
    Public Sub TestGetOperatorCalc_OperationAddition()
        Dim op As String = "+"
        Dim x As Single = 1
        Dim y As Single = 2
        Dim expectedResult As Single = 3
        Dim actualResult As Single = GetOperatorCalc(op, x, y)

        Assert.AreEqual(expectedResult, actualResult)
    End Sub

    <TestMethod()>
    Public Sub TestGetOperatorCalc_NegativeOperation()
        Dim op As String = "E"
        Dim x As Single = 1
        Dim y As Single = 2

        Try
            GetOperatorCalc(op, x, y)
        Catch ex As ArgumentException
            Return
        Catch
            Assert.Fail("Ошибка исключения для недопустимой операции")
            Return
        End Try

        Assert.Fail("Нет исключения на неправильные исходные данные")
    End Sub

    <TestMethod()>
    Public Sub TestCalc()
        Dim inputArrayList As New ArrayList
        inputArrayList.Add("8")
        inputArrayList.Add("2")
        inputArrayList.Add("5")
        inputArrayList.Add("*")
        inputArrayList.Add("+")
        inputArrayList.Add("1")
        inputArrayList.Add("3")
        inputArrayList.Add("2,5")
        inputArrayList.Add("*")
        inputArrayList.Add("+")
        inputArrayList.Add("4")
        inputArrayList.Add("-")
        inputArrayList.Add("/")

        Dim expectedResult As Single = 4
        Dim actualResult As Single = Calc(inputArrayList)

        Assert.AreEqual(expectedResult, actualResult)
    End Sub

    <TestMethod()>
    Public Sub TestCalc_NegativeOperation()
        Dim inputArrayList As New ArrayList
        inputArrayList.Add("E")

        Try
            Calc(inputArrayList)
        Catch ex As ArgumentException
            Return
        Catch
            Assert.Fail("Ошибка исключения для недопустимой операции")
            Return
        End Try

        Assert.Fail("Нет исключения на неправильные исходные данные")
    End Sub

End Class
