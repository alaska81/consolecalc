﻿Imports System.Text.RegularExpressions

Module ModuleMain
    Sub Main()
        Dim inputString As String

        Console.WriteLine("Введите выражение:")
        inputString = Console.ReadLine()

        Try
            inputString = inputString.Replace(".", ",")
            'Dim resultArray As ArrayList = StringToArrayList(inputString)
            Dim resultMaches As MatchCollection = StringToMatches(inputString)
            Dim resultBackPolskRec As ArrayList = BackPolskRec(resultMaches)
            Dim result As String = Calc(resultBackPolskRec).ToString
            Console.WriteLine("Результат: " & result)
            Console.ReadKey()
        Catch ex As Exception
            Console.WriteLine("Ошибка выражения!")
            Console.ReadKey()
        End Try

    End Sub

    ''Конвертер строки в ArrayList
    'Public Function StringToArrayList(ByVal inputString As String) As ArrayList
    '    'Throw New NotImplementedException()
    '    Dim result As New ArrayList
    '    Dim inputStringArray As System.Array = inputString.ToArray
    '    Dim tmpString As String = ""

    '    If inputStringArray.Length > 0 Then
    '        For Each el As String In inputStringArray
    '            If IsNumeric(el) Or el = "," Then
    '                tmpString &= el
    '            Else
    '                If tmpString <> "" Then
    '                    result.Add(tmpString)
    '                    tmpString = ""
    '                End If

    '                result.Add(el)
    '            End If
    '        Next
    '        If tmpString <> "" Then
    '            result.Add(tmpString)
    '        End If
    '    End If

    '    Return result
    'End Function

    Public Function StringToMatches(ByVal inputString As String) As MatchCollection
        Dim newReg As Regex = New Regex(pattern)

        Dim result As MatchCollection = newReg.Matches(inputString)
        Return result
    End Function

    'Обратная польская запись
    Public Function BackPolskRec(inputMatchCollection As MatchCollection) As ArrayList
        Dim result As New ArrayList
        Dim tmpStack As New Stack

        For Each match As Match In inputMatchCollection
            Dim op As String = match.Value

            If IsNumeric(op) Then
                result.Add(op)
            Else
                Select Case op
                    Case "("
                        tmpStack.Push(op)

                    Case ")"
                        Do
                            If tmpStack.Count > 0 Then
                                If tmpStack.Peek = "(" Then Exit Do
                                result.Add(tmpStack.Pop)
                            Else
                                Throw New ArgumentException
                            End If
                        Loop
                        tmpStack.Pop()

                    Case Else
                        While tmpStack.Count > 0 AndAlso GetOperatorPrioritet(op) <= GetOperatorPrioritet(tmpStack.Peek)
                            result.Add(tmpStack.Pop)
                        End While

                        tmpStack.Push(op)
                End Select
            End If
        Next

        While tmpStack.Count > 0
            If tmpStack.Peek <> ")" And tmpStack.Peek <> "(" Then
                result.Add(tmpStack.Pop)
            Else
                Throw New ArgumentException
            End If
        End While

        Return result
    End Function

    'Вычисления
    Public Function Calc(inputArrayList As ArrayList) As Single
        Dim result As Single = 0
        Dim tmpStack As New Stack

        For Each op As String In inputArrayList
            If IsNumeric(op) Then
                tmpStack.Push(op)
            Else
                If tmpStack.Count >= 2 Then
                    Dim x As Single = tmpStack.Pop
                    Dim y As Single = tmpStack.Pop
                    Dim z As Single = GetOperatorCalc(op, x, y)

                    tmpStack.Push(z)
                Else
                    Throw New ArgumentException
                End If
            End If
        Next
        result = tmpStack.Pop

        Return result
    End Function

End Module

