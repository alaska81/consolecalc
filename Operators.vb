﻿Public Class OperationAddition
    Public Function Result(ByVal x As Single, ByVal y As Single) As Single
        Return x + y
    End Function
End Class

Public Class OperationSubtraction
    Public Function Result(ByVal x As Single, ByVal y As Single) As Single
        Return y - x
    End Function
End Class

Public Class OperationMultiplication
    Public Function Result(ByVal x As Single, ByVal y As Single) As Single
        Return x * y
    End Function
End Class

Public Class OperationDivision
    Public Function Result(ByVal x As Single, ByVal y As Single) As Single
        Return y / x
    End Function
End Class

'Public Class OperationExponentiation
'    Public Function Result(ByVal x As Single, ByVal y As Single) As Single
'        Return y ^ x
'    End Function
'End Class

Public Module ModuleOperators
    Public pattern As String = "(\d+\,?\d*)|(\(|\))|(\+|\-|\*|\/)"
    Public Function Operators() As ArrayList
        Dim OperatorsArray As New ArrayList

        OperatorsArray.Add(New Symbol("(", 0))
        OperatorsArray.Add(New Symbol(")", 0))
        OperatorsArray.Add(New Symbol("+", 1, New OperationAddition))
        OperatorsArray.Add(New Symbol("-", 1, New OperationSubtraction))
        OperatorsArray.Add(New Symbol("*", 2, New OperationMultiplication))
        OperatorsArray.Add(New Symbol("/", 2, New OperationDivision))
        'OperatorsArray.Add(New Symbol("^", 3, New OperationExponentiation))

        Return OperatorsArray
    End Function

End Module

