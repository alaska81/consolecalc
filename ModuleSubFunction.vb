﻿Public Module ModuleSubFunction

    'Получаем приоритет операции по символу
    Public Function GetOperatorPrioritet(ByVal Symbol As String) As Integer

        For Each op As Symbol In Operators()
            If op.Symbol = Symbol Then
                Return op.Prioritet
            End If
        Next
        Throw New ArgumentException

    End Function

    'Выполненяем вычисления по символу операции
    Public Function GetOperatorCalc(ByVal Symbol As String, ByVal x As Single, ByVal y As Single) As Single

        For Each op As Symbol In Operators()
            If op.Symbol = Symbol Then
                Return op.Calc(x, y)
            End If
        Next
        Throw New ArgumentException

    End Function

End Module

