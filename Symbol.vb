﻿Public Class Symbol
    Dim _Symbol As String
    Dim _Prioritet As Integer
    Dim _Operation As Object

    Public ReadOnly Property Symbol As String
        Get
            Return _Symbol
        End Get
    End Property

    Public ReadOnly Property Prioritet As String
        Get
            Return _Prioritet
        End Get
    End Property

    Public Sub New(ByVal Symbol As String, ByVal Prioritet As Integer)
        _Symbol = Symbol
        _Prioritet = Prioritet
    End Sub

    Public Sub New(ByVal Symbol As String, ByVal Prioritet As Integer, ByVal Operation As Object)
        _Symbol = Symbol
        _Prioritet = Prioritet
        _Operation = Operation
    End Sub

    Public Function Calc(x, y) As Single

        Return _Operation.Result(x, y)
    End Function

End Class
